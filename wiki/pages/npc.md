## Персонажи

Все неигровые персонажи восстанавливаются на сервере.

### Восстновление

Восстновление всех персонажей происходит по таймеру в течении 30 минут. Но для того, чтобы персонаж возрадился, необходимо покинуть ячейку и войти в неё вновь.

### Счётчик убийств

Начиная с 16 октября 2018 года счётчик убийств был сделан для каждого свой (чтобы вы понимали до этого он был общий). Причиной тому были технические трудности с некоторыми квестами, например квестами Гильдии Бойцов. Потенциально это избавляет от проблем во всех квестах.

После обсуждения вопроса было решено не добавлять к счётчику всех важных персонажей (у которых появляется надпись о заваленом квесте, так как де-факто они бессмертны). Их id таковы:

Персонаж |
----------------|
almalexia|
BM_horker_swim_UNIQUE|
dwarven ghost_radac|
yagrum bagam|
addhiranirr|
apelles matius|
asciene rane|
athyn sarethi|
berenziah|
Blatta Hateria|
brara morvayn|
caius cosades|
crassius cuiro|
crazy_batou|
danso indules|
divayth fyr|
dram bero|
dutadalk|
effe_tei|
endryn liethan|
falura llervu|
falx carius|
fedris hler|
garisa llethri|
gavas drin|
gilvas barelo|
han-ammu|
haspat antabolis|
hassour zainsubani|
hlaren ramoran|
huleeya|
karrod|
kaushad|
kausi|
King Hlaalu Helseth|
lord cluttermonkey|
manirai|
mehra milo|
miner arobar|
nevena ules|
nibani maesa|
raesa pullia|
savile imayn|
sharn gra-muzgob|
sinnamu mirpal|
sonummu zabamat|
sul-matuul|
tharsten heart-fang|
tholer saryoni|
Tienius Delitian|
uupse fyr|
varvur sarethi|
velanda omani|
yenammu|
