# Freedom Land

Site generator, template, wiki, blog for Freedom Land. 

Old site [here](https://gitlab.com/freedomland/website-old).

### Third-party software, templates, css...

- Heavy modified "Produkta" template by Azmind.com.
- [Pymdownx](https://github.com/facelessuser/pymdown-extensions/) by facelessuser
- Twitter Bootstrap by Twitter Inc. @mdo and @fat
- jQuery by jQuery Foundation
- CSS3 Pricing tables by Anli Zaimi
- PrettyPhoto by no-margin-for-errors.com
- Font Awesome Icons by Font Awesome

For more information see public/Attributions.txt

### License
Freedom Land website (c) 2019 Volk_Milit (aka Ja'Virr-Dar), CC-BY-SA 3.0
