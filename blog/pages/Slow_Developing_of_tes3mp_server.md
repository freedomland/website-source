## "Слишком долго обновляетесь!", или как выпустить обновление для сервера tes3mp

"А не сделать ли нам свой сервер?", задумались мы. И сделали. Одни из первых кто сделал такие штуки как восстановление NPC (ака не играбельных персонажей) на сервере, написали лаунчер для лёгкой установки и обновления игрового контента, исправили проклятые предметы на алтарях даэдра, исправили вампиризм, сделали убийства NPC для каждого игрока отдельно (а не единые для всех, как и сейчас в оригинахльных CoreScripts). Артефакты, которые игрок может брать лишь единожды за одного персонажа, игроки как учителя, арена и ещё 21 [скрытых репозиториев](https://gitlab.com/freedomland). Но всегда найдётся ложка дёгтя в бочке с мёдом.

Когда мы только начали разрабатывать (*это уже было год назад, кстати*), мы столкнулись с **таким** колличеством проблем, которые даже не могли себе представить. Начиная от сломаных квестов, заканчивая тем, что tes3mp просто не предназначен для мультиплеера в текущем его виде. А допиливать напильником долгое и нудное занятие, особенно когда в команде только два активно работающих над этим человека.

И тут появляются **они**. Всепропальщики.

Их тезисы заключаются в следующем:

- Вы слишком долго обновляетесь, это плохо для репутации сервера.
- Зачем вы делаете ещё один сервер?!
- Зачем вы вообще живёте?
- Донат, донат, донат... (которого нет)
- Никто не занимается мультиплеером а вы пытаетесь сделать что-то не глючащее и не падающее каждые две секунды.
- Донат!!!1111
- Вещи за деньги!!!11111111!!11!1
- Я уверен что у вас будут вещи за деньги! Админки за деньги!! Ко-ко-ко.
- Вещи, которые легко достать теперь ещё легче достать.

Попытаюсь развеять все эти мифы.

### Вы слишком долго обновляетесь, это плохо для репутации сервера

Только что этот же человек говорит тебе, что мультиплеером никто не занимается, а мы делаем фигню и вообще надо бы уже открываться. Объясняю зачем вообще понадобился ЗБТ.

Когда мы делали ММО на 0.6.2, то не знали к чему вообще стремимся. Было много дурацких идей, некоторые идеи так и не были воплощены в жизнь. С приходом 0.7.0 ситуация немного изменилась, однако всё-равно ограничения не дают нормально развиваться и двигаться в правильном направлении.

JSON не подходит для мультиплеера, все профили игроков, все ячейки (хоть они и хранят изменение игрового мира и только его), записываются в JSON; что вероятно вызывает микро-фризы. Сразу говорю, что SQL так никто и не сделал, а если этим начну заниматься я, то про сервер можно забыть. Ситуация немного изменилась когда был портирован CJSON (руки чесались самому сделать, но ребята уже постарались). Запись и чтение стали быстрее, я перевёл все апи на него и более не пишу в человеко-читаемом формате.

UDP -- корень зла, потеря пакетов, пропавшие вещи, лаги; знакомая ситуация на 0.6.2, не правда ли? Когда мы Стасом начинали этот проект, я говорил что с UDP будут проблемы. Я не ошибся, но каждый так и остался при собственном мнении. "Проект исследовательский", говорил он. Ну, что тут можно сказать?

В связи со всем этим мы решили более не распыляться и потихоньку портироваться на 0.7.0, попутно переписав многие вещи. Так был практически полностью переписан RealEstate и все наши плагины. Да, у нас нет других сторонних плагинов. Более того, мы форкнули CoreScripts и адаптировали под мултиплеер. Причина почему мы его не открываем в том, что там сплошные хаки и скорее всего Девид сделает лучше. Важно то, что это работает.

После чего мы столкнулись с некоторыми проблемами технического характера. А именно с дурацким форматом esm\esp (ака Elder Scrolls Master\Plugin). Теперь мы стараемся делать минимально на клиенте и переносим многие вещи на сервер.

Ещё одна причина по которой мы не торопимся заключается в том что **у нас есть реальная жизнь**. Да, представьте себе, у нас есть работа, семья и заботы, мы занимаемся проектом в свободное время. Мы платим за сервер **из своих денег**. Это приводит нас к следующему тезису.

З.Ы. Этот же тезис можно применить к любому Open Source проекту. Если хотите нам помочь -- помогайте. Если нечего сказать -- молчите.

### Донат

Зачем нужен донат? Для поддержания сервера в рабочем состоянии. Верите или нет, но мы содержим его на свои кровно заработанные деньги. Мы **никогда** не будем продавать админки или вещи за деньги. Это всё, что я хотел сказать.

### Зачем вы делаете ещё один сервер?

Мы всего-лишь второй русскоязычный сервер и никогда особо не планировали делать что-то для англоязычной аудитории. Сервер ориентирован для русскоязычных игроков, а выбора у тех, кто не знает английского особо нет. Собственно поэтому и родилась идея сервера. К тому же я не видел чтобы кто-то делал именно ММО, везде мультиплеер с непонятным лором. Наш лор всецело полагается на концепты Бесезды.

### Никто не занимается мультиплеером

Достаточно зайти на [tes3mp.com](https://tes3mp.com) и посмотреть сколько серверов и игроков сейчас. Достаточно зайти в браузер (версий 0.6.2 и 0.7.0) и посмотреть сколько там открытых серверов. Достаточно зайти в дискорд TES3MP и найти там множество независимых разработчиков. Если же речь про сам мультиплеер, то я бухаю со Стасом каждую неделю и каждую неделю он показывает что-то новое. Если и это вас не убедило, то я не знаю что убедит.

### Вещи, которые легко достать теперь ещё легче достать

С этим тезисом я действительно соглашусь. Это была проблема старого Freedom Land, новый Freedom Land этой ошибки не повторит. Хочешь крутую шмотку? Будь добр победить босса. К тому же шмотки теперь выдаются для всех игроков, которые побеждали босса вместе. Второй раз победив этого босса игрок не получит ничего, кроме, возможно, опыта. Во время боя рандомный игрок не сможет пройти на арену к боссу. Система скопированна с Dark Souls, если вы знаете о чём я.

### Заключение

Этот небольшой пост посвящается всем всепропальщикам и засланным казачкам. Как их отличить от обычных нетерпеливых игроков? Обычно их показания сильно разнятся: *"Мультиплеером никто не занимается"* и тут же *"Зачем нам ещё один сервер?"*. 

Если вы думаете что отвратите нас от разработки таким образом, вам нужно стараться лучше. Даже если в это никто не будет играть (как показала практика, полнейшая чушь, всё-равно находятся игроки), это войдёт в наше портфолио. А оно очень ценится в среде IT, где я работаю.

---

– © Volk_Milit (aka Ja'Virr-Dar) 2019, [CC-BY-ND](https://creativecommons.org/licenses/by-nd/4.0/legalcode).
