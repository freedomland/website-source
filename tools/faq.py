#!/usr/bin/python

import os, sys
from io import open

def formater(line, num):
	if line[0] == '-':
		return '<li><a href="#' + str(num) + '">' + line.split('- ')[1] + '</a></li>'
	elif line[0] == '*':
		return '<p id="' + str(num) + '"><b>Q: </b> ' + line.split('* ')[1] + '<br>'
	elif line[0] == '&':
		return '<b>A: </b> ' + line.split('& ')[1] + '</p>'
	elif not line.strip():
		return '<br>'
	else:
		return '<br>'

templateFile = open('template-faq.html', 'r', encoding='utf-8')
template = templateFile.read()
templateFile.close()
	
content = ''
	
contentStart = False
contentEnd = False
contentNum = -1
	
zaglushka = '<script>$(".container").css("display", "none");$(document).ready(function(){$("a.zaglushka").click(function(event){$("#zaglushka").css("display", "none");$(".container").css("display", "block");});});</script>'
select = """<script src="assets/js/jquery-1.8.2.min.js"></script>
        <script>			
			$(document).ready(function()
			{
				var count = $("#index").find("li").length;
				var url = window.location.hash;
				var hash = url.substring(url.indexOf("#")+1);
				
				$("#" + hash).css("background", "#FFEAC0");
				
				$(document).on("click", "a", function()
				{
					for (i = 0; i < count; i++)
					{
						$("#" + i).css("background", "#FFFFFF");
					}
					
					var id =  $(this).attr("href");
					$(id).css("background", "#FFEAC0");
				});
			});
        </script>"""
	
with open('faq.txt', 'r', encoding='utf-8') as f:
	for line in f:
		if line[0] == '-' and contentStart == False:
			content += '<ul id="index">'
			contentStart = True
			
		if not line.strip() and contentEnd == False:
			content += '</ul><hr><span class="faq">'
			contentEnd = True
			contentNum = 0
			continue
			
		if not line.strip() or line[0] == '-':
			contentNum += 1
				
		content += formater(line, contentNum)
			
content += '</span>'
	
print(template.format('Часто задаваемые вопросы', select, content, zaglushka))
