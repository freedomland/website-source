#!/usr/bin/python

import markdown
import markdown.extensions
import os, sys
import pymdownx

from io import open

if os.path.exists(sys.argv[1]):
	mkin = open(sys.argv[1], 'r', encoding='utf-8')
	title = mkin.readline()
	content = markdown.markdown(mkin.read(), extensions=['pymdownx.tilde', 'markdown.extensions.tables', 'markdown.extensions.fenced_code'])
	mkin.close()
	
	templateFile = open('template.html', 'r', encoding='utf-8')
	template = templateFile.read()
	templateFile.close()

	titleReal = title.split("##")[1]

	zaglushka = '<script>$(".container").css("display", "none");$(document).ready(function(){$("a.zaglushka").click(function(event){$("#zaglushka").css("display", "none");$(".container").css("display", "block");});});</script>'

	print(template.format(titleReal, content, zaglushka))
	
else:
	print('Wrong command')
